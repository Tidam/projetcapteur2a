import click
from .app import app , db

@app.cli.command()
def syncdb():
    '''creer table manquante'''
    db.create_all()

@app.cli.command()
@click.argument('nomPers')
@click.argument('prenomPers')
@click.argument('mailPers')
@click.argument('telPers')
@click.argument('photoProfilPers')
@click.argument('mdpPers')
def newuser(nomPers, prenomPers, mailPers , telPers ,photoProfilPers, mdpPers):
    '''Adds a new user.'''
    from .models import Personne
    from hashlib import sha256
    m = sha256()
    m.update( password.encode())
    p = Personne( nomPers=nomPers, prenomPers=prenomPers, mailPers=mailPers, telPers=telPers, photoProfilPers=photoProfilPers, mdpPers=m.hexdigest())
    db.session.add(p)
    db.session.commit()


@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    db.create_all()

    import yaml
    capteur = yaml.safe_load(open(filename))
    from .models import Donnees, capteur, SuperAdmin

    
