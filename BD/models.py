from flask_sqlalchemy import SQLAlchemy 
from datetime import datetime
from wtforms import Form, BooleanField, StringField, PasswordField, validators

"""création d'une instance flask"""

app = Flask(__name__)

"""l'ajout de la BD"""

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///todo.db'  #c'est pour dire où se trouve notre BD
#et qu'on utilise sqlite et le todo.db est le nom de la BD 

""" l'initialisation de la BD""" 

db = SQLAlchemy(app) # on a initialisé l'instance db*/

"""la création des classes (tables)"""


# many to one entre pbutilisateur et personne 
# one to many entre abonnement et personne 
# many to many entre capteur et personne et groupe et personne 
# one to many entre capteur et donnees




class Donnees(db.Model): #enfant
    idDo = db.Column(db.Integer, primary_key = True, autoincrement=True)
    dateDo = db.Column(db.DateTime, nullable = False, default = datetime.utcnow)
    donneeBrutDo =  db.Column(db.String(700), nullable = False)
    donneeTransfoDo =  db.Column(db.String(800), nullable = False)
    idCap = db.Column(db.Integer, nullable = False, unique= True)
    cap_id = db.Column(db.Integer, ForeignKey('capteur.idCap'))

class capteur(db.Model):  #on hérite les infos de db.Model A REVOIR  #parent
    idCap = db.Column(db.Integer, primary_key = True, autoincrement=True)
    nomCap = db.Column(db.String(80), nullable = False) #nullable = False on accepte pas un Cap sans nom
    infoCap = db.Column(db.String(500), nullable = False)
    typeCap = db.Column(db.String(80), nullable = False)
    geolocalCap = db.Column(db.String(200), nullable = False)
    statusCap = db.Column(db.String(150), nullable = False)
    derConsultation = db.Column(db.DateTime, nullable = False, default = datetime.utcnow)
    PersoPosdCap = relationship("Personne",secondary= "Utiliser", back_populates = "capteurPerso")
    donneeCap = relationship("Donnees")

Utiliser = db.Table('Utiliser', db.Column('personne_idPers',  db.Integer,
                                        ForeignKey('personne.idPers')),
                                db.Column('capteur_idCap',db.Integer,
                                        ForeignKey('capteur.idCap'))
)

class Personne(db.Model): #parent
    idPers = db.Column(db.Integer, primary_key = True, autoincrement=True)
    mailPers = db.Column(db.String(150), nullable = False, unique= True)
    telPers = db.Column(db.Integer, nullable = False)
    nomPers = db.Column(db.String(80), nullable = False)
    prenomPers = db.Column(db.String(80), nullable = False)
    photoProfilPers = db.Column(db.String(500), nullable = False) #pas tres sure pour la photo de profil 
    mdpPers = db.Column(db.Integer, nullable = False)
    groupePerso = relationship("Groupe",secondary= "Etre_dans", back_populates = "perso")
    capteurPerso = relationship("capteur",secondary= "Utiliser", back_populates = "PersoPosdCap")

    pbPerso_id = db.Column(db.String(80), ForeignKey('problemeUtilisateur.sujetPb'))
    pbPerso = relationship("problemeUtilisateur")

    superAdminPerso = db.relationship("superAdmin", back_populates = "persoAdmin")
    persoUtilisateur = db.relationship("utilisateur", back_populates = "utilisateurperso")

Etre_dans = db.Table('Etre_dans',db.Column('personne_idPers',  db.Integer,
                                            ForeignKey('personne.idPers')),
                                db.Column('groupe_idG', db.Integer,
                                            ForeignKey('groupe.idG'))
)

class Groupe(db.Model):
    idG = db.Column(db.Integer, primary_key = True)
    nomG = db.Column(db.String(80), nullable = False)
    nbpersG = db.Column(db.Integer)
    photoProfilG = db.Column(db.String(500), nullable = False)
    descripProfilG =  db.Column(db.String(700), nullable = False)
    adminG = db.Column(db.Integer, nullable = False)
    perso = db.relationship("Personne",secondary= "Etre_dans", back_populates = "groupePerso")

# Posseder = Table('Posseder',db.Column('personne_idPers', db.Integer,
#                                           ForeignKey('personne.idPers')), ???? 
#                             db.Column('abonnement_dateDebutAbo',db.DateTime),
#                             db.Column('abonnement_dateFinAbo',db.DateTime),
#                             db.Column('abonnement_idAbo', db.Integer, ForeignKey('abonnement.idAbo'))
# )

class Abonnement(db.Model):
    idAbo = db.Column(db.Integer, primary_key = True, autoincrement=True)
    nomAbo = db.Column(db.String(80), nullable = False)
    prixAbo = db.Column(db.Integer, nullable = False)
    infoAbo = db.Column(db.String(700), nullable = False)
    dateDebutAbo = db.Column(db.DateTime, nullable = False, default = datetime.utcnow)
    dateFinAbo = db.Column(db.DateTime, nullable = False, default = datetime.utcnow)
    persPosdAbo = relationship('Personne')
    personne_id = Column(Integer, ForeignKey('personne.idPers'))

class ProblemeUtilisateur(db.Model) : 
    sujetPb = db.Column(db.String(80), nullable = False,  primary_key = True)
    messagePb = db.Column(db.String(80), nullable = False)
    datePb = db.Column(db.DateTime, nullable = False, default = datetime.utcnow)
    idPers = Column(Integer, ForeignKey('personne.idPers'))
    pbPersonne = 


class SuperAdmin(db.Model):
    idSA = db.Column(db.Integer, primary_key = True, autoincrement=True)
    nomSA = db.Column(db.String(80), nullable = False)
    idPers = db.Column(db.Integer, nullable = False)
    persoAdmin = db.relationship("Personne",back_populates = "superAdminPerso")

class Utilisateur(db.Model):
    idU = db.Column(db.Integer, primary_key = True, autoincrement=True)
    nomU = db.Column(db.String(80), nullable = False)
    AdminU = db.Column(db.String(80), nullable = False)
    idPers = db.Column(db.Integer, nullable = False)
    utilisateurperso = db.relationship("Personne", back_populates = "persoUtilisateur")


class LoginForm(FlaskForm):
    prenom = StringField("Prenom", validators=[DataRequired()])
    nom = StringField("Nom", validators=[DataRequired()])
    tel = StringField('numero de telephone', [validators.Length(min=10, max=10, message="numero doit se composer de 10 nombre (sans espace)")])
    email = StringField("Email", validators= [DataRequired()])
    mdp =PasswordField('Mot de passe', [
        validators.DataRequired(),
        validators.EqualTo('confirmMdp', message='mot de passe doit etre identique')])
    confirmMdp = PasswordField('confirmez le mot de passe')
    submit = SubmitField("Submit")



created_at = db.Column(db.DateTime, nullable = False, default = datetime.utcnow)

@app.route('/user/add', methods = ['GET', 'POST'])
def add_user():
    form = UserForm()
    if form.validate_on_submit():
        user = Personne.query.filter_by(email = form.email.data).first()
        if user is None :
            user = Personne(email = form.email.data, prenom = form.prenom.data, nom = form.nom.data, mdp = form.mdp.data, photoProfilPers tel = form.tel.data)
            db.session.add(user)
            db.session.commit()
        nom = form.nom.data
        form.nom.data = ''
        form.email.data = ''
        flash("utilisateur ajouté")
    return render_template("add_user.html", form = form, nom = nom)


"""class UploadForm(FlaskForm):
    upload = FileField('image', validators=[
        FileRequired(),
        FileAllowed(['jpg', 'png'], 'Images only!')
    ])"""