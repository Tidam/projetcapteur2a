
CREATE TABLE Personne(
   idPers VARCHAR(50),
   mailPers VARCHAR(50),
   telPers VARCHAR(50),
   nomPers VARCHAR(50),
   prenomPers VARCHAR(50),
   photoProfilPers VARCHAR(50),
   mdpPers VARCHAR(50),
   PRIMARY KEY(idPers)
);

CREATE TABLE Abonnement(
   idAbo VARCHAR(10),
   nomAbo VARCHAR(40),
   prixAbo INT,
   infoAbo VARCHAR(240),
   dateDebutAbo VARCHAR(100),
   dateFinAbo(100),
   PRIMARY KEY(idAbo)
);

CREATE TABLE Groupe(
   idG VARCHAR(50),
   nomG VARCHAR(50),
   nbpersG INT,
   photoProfilG VARCHAR(50),
   descriptionG VARCHAR(240),
   adminG VARCHAR(50),
   PRIMARY KEY(idG)
);

CREATE TABLE SuperAdmin(
   idSA VARCHAR(10),
   nomSA VARCHAR(20),
   idPers VARCHAR(50) NOT NULL,
   PRIMARY KEY(idSA),
   FOREIGN KEY(idPers) REFERENCES Personne(idPers)
);

CREATE TABLE Capteur(
   idC VARCHAR(10),
   nomC VARCHAR(30),
   infoC VARCHAR(100),
   typeC VARCHAR(50),
   statutC VARCHAR(20),
   geolocalisationC TEXT,
   derConsultationC DATE,
   PRIMARY KEY(idC)
);

CREATE TABLE Donnees(
   idDo VARCHAR(30),
   dateDo DATETIME,
   donneebruteDo VARCHAR(50),
   donneetransfoDo VARCHAR(50),
   idC VARCHAR(10) NOT NULL,
   PRIMARY KEY(idDo),
   FOREIGN KEY(idC) REFERENCES Capteur(idC)
);

CREATE TABLE Utilisateur(
   idU VARCHAR(10),
   nomU VARCHAR(20),
   AdminU LOGICAL,
   idPers VARCHAR(50) NOT NULL,
   PRIMARY KEY(idU),
   FOREIGN KEY(idPers) REFERENCES Personne(idPers)
);

CREATE TABLE ProblemeUtilisateur(
   sujetPb VARCHAR(50),
   messagePb VARCHAR(240),
   datePb DATE,
   idPers VARCHAR(50),
   PRIMARY KEY(sujetPb),
   FOREIGN KEY(idPers) REFERENCES Personne(idPers)
);

CREATE TABLE Utiliser(
   idPers VARCHAR(50),
   idC VARCHAR(10),
   PRIMARY KEY(idPers, idC),
   FOREIGN KEY(idPers) REFERENCES Personne(idPers),
   FOREIGN KEY(idC) REFERENCES Capteur(idC)
);

CREATE TABLE Etre_dans(
   idPers VARCHAR(50),
   idG VARCHAR(50),
   PRIMARY KEY(idPers, idG),
   FOREIGN KEY(idPers) REFERENCES Personne(idPers),
   FOREIGN KEY(idG) REFERENCES Groupe(idG)
);

CREATE TABLE Posseder(
   idPers VARCHAR(50),
   date_debut DATE,
   date_fin DATE,
   idAbo VARCHAR(10) NOT NULL,
   PRIMARY KEY(idPers),
   FOREIGN KEY(idPers) REFERENCES Personne(idPers),
   FOREIGN KEY(idAbo) REFERENCES Abonnement(idAbo)
);
