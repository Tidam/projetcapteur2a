import yaml, os.path 
from .app import db, login_manager
from flask_login import UserMixin
from datetime import datetime
from sqlalchemy import ForeignKey, UniqueConstraint

class Capteur(db.Model):  #on hérite les infos de db.Model A REVOIR  #parent
    idCap = db.Column(db.Integer, primary_key = True, autoincrement=True)
    nomCap = db.Column(db.String(80), nullable = False) #nullable = False on accepte pas un Cap sans nom
    infoCap = db.Column(db.String(500), nullable = False)
    typeCap = db.Column(db.String(80), nullable = False)
    geolocalCapX = db.Column(db.Float(200), nullable = True)
    geolocalCapY = db.Column(db.Float(200), nullable = True)
    statusCap = db.Column(db.String(150), nullable = False)
    derConsultation = db.Column(db.DateTime, nullable = True, default = datetime.utcnow())

class Donnees(db.Model): #enfant
    idDo = db.Column(db.Integer, primary_key = True, autoincrement=True)
    dateDo = db.Column(db.DateTime, nullable = False, default = datetime.utcnow())
    donneeBrutDo =  db.Column(db.String(700), nullable = False)
    donneeTransfoDo =  db.Column(db.String(800), nullable = False)
    idCapt = db.Column(db.Integer, ForeignKey("capteur.idCap"))

class Abonnement(db.Model):
    idAbo = db.Column(db.Integer, primary_key = True, autoincrement=True)
    nomAbo = db.Column(db.String(80), nullable = False)
    prixAbo = db.Column(db.Integer, nullable = False)
    infoAbo = db.Column(db.String(700), nullable = False)
    dateDebutAbo = db.Column(db.DateTime, default = datetime.utcnow())
    dateFinAbo = db.Column(db.DateTime, default = datetime.utcnow())

liason_pers_groupe = db.Table('pers_groupe', db.metadata,
    db.Column('pers_id', ForeignKey('personne.idPers')),
    db.Column('gr_id', ForeignKey('groupe.idG')),
    UniqueConstraint('pers_id', 'gr_id', name='pers_dans_grp')
)

class Personne(db.Model, UserMixin): #parent
    idPers = db.Column(db.Integer, primary_key = True, autoincrement=True)
    mailPers = db.Column(db.String(150), nullable = False, unique= True)
    nomPers = db.Column(db.String(80), nullable = False)
    prenomPers = db.Column(db.String(80), nullable = False)
    photoProfilPers = db.Column(db.String(500)) #pas tres sure pour la photo de profil 
    mdpPers = db.Column(db.String(64), nullable = False)
    rolePers = db.Column(db.String(80), nullable = False)
    aboId = db.Column(db.Integer, ForeignKey("abonnement.idAbo"))

    def get_id(self):
        return self.idPers

    @login_manager.user_loader
    def load_user(nomPers):
        return Personne.query.get(nomPers)

    def get_mdp(self):
        return self.mdpPers


class Groupe(db.Model):
    idG = db.Column(db.Integer, primary_key = True)
    nomG = db.Column(db.String(80), nullable = False)
    nbpersG = db.Column(db.Integer)
    photoProfilG = db.Column(db.String(500), nullable = False)
    descripProfilG =  db.Column(db.String(700), nullable = False)
    AdminU = db.Column(db.String(80), nullable = False)
    
    

class ProblemeUtilisateur(db.Model) : 
    sujetPb = db.Column(db.String(80), nullable = False,  primary_key = True)
    messagePb = db.Column(db.String(80), nullable = False)
    datePb = db.Column(db.DateTime, default = datetime.utcnow())
    idpers = db.Column(db.Integer, ForeignKey("personne.idPers"))
  
    
def get_catpeur_par_pers(nom):
    return Capteur.query.filter(Personne.nomPers == nom).all()


def get_capteur_par_id(id):
    return Capteur.query.filter(Capteur.idCap == id).first()

def get_abo_par_uti(id):
    return Abonnement.query.filter(Abonnement.personne_id == id).first()

def get_groupes_par_pers(nom):
    return Groupe.query.filter(Personne.nomPers == nom).all()

def get_capt_par_gr(id):
    return Capteur.query.filter(Groupe.idG == id).all()

def get_capt_all():
    return Capteur.query.all()