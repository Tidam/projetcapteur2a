from datetime import datetime
from .app import app, db
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, PasswordField, SubmitField
from flask import render_template, url_for, redirect, request
from .models import *
from wtforms import PasswordField, SubmitField
from hashlib import sha256
from flask_login import login_user, current_user, logout_user, login_required
from wtforms.validators import DataRequired, length, EqualTo


class LoginForm(FlaskForm):
    email = StringField('email',validators = [DataRequired(), length(min=3, max=20)], render_kw = {"placeholder" : "E-mail"})
    motDePasse = PasswordField('Password',validators = [DataRequired(), length(min=4, max=16)], render_kw = {"placeholder" : "Mot de passe"})
    seConnecter = SubmitField("Se connecter")
    next = HiddenField()

    def get_authenticated_user(self):
        print(self.email.data)
        user = Personne.query.filter(Personne.mailPers == self.email.data).first()

        if user is None:
            return None
        # m = sha256()
        # m.update(self.motDePasse.data.encode())
        # passwd = m.hexdigest()
        if self.motDePasse.data == user.get_mdp():
            return user
        else:
            return False


@app.route("/", methods=("GET",))
def accueil():
    return render_template(
        "accueil.html",
        title = "Green Sensor",
        titreCarte1 = "Bienvenue sur Green Sensor !",
        date = datetime.now().strftime("%d/%m/%y"),
        titreCarte2= "Utilisation d'une technologie Arduino",
        formuleGratuite=["Gratuite","Avec pub","5 capteurs disponibles","Acccès au site web"],
        formuleStandard=["28.60€/mois","Sans pub","15 capteurs disponibles","Installation","Maintenance","Localisation disponible"],
        formuleProfessionelle=["70.90€/mois","Sans pub","50 capteurs disponibles","Installation","Maintenance","Localisation disponible","+ d'option d'analyse"]
    )

@app.route("/Connexion", methods=("GET","POST",))
def connexion_get():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    if f.validate_on_submit():
        print("validate")
        user = f.get_authenticated_user()
        if user:
            print("CONNECTE")
            login_user(user)
            next = f.next.data or url_for ("accueil")
            return redirect(url_for("accueil"))   #changer ici pour la page accueil avec le menu hamburger
    return render_template(
        "connexion.html",
        title = "Se connecter",
        form = f
        )

class RegistrationForm(FlaskForm):
    prenom = StringField(label = 'prenom', validators = [DataRequired(), length(min=3, max=20)])
    nom = StringField(label = 'nom', validators = [DataRequired(), length(min=3, max=20)])
    email = StringField(label = 'Email', validators= [DataRequired(), length(min=10, max=20)])
    password = PasswordField(label = 'Password',validators = [DataRequired(), length(min=6, max=16)])
    confirm_password = PasswordField(label = 'Password',validators = [DataRequired(),EqualTo('password')])
    submit = SubmitField('Se connecter')


@app.route("/Inscription", methods=("POST","GET",))
def inscription_get():
    prenom = request.form.get('prenom')
    nom = request.form.get('nom')
    email = request.form.get('email')
    mdp = request.form.get('password')
    confirm = request.form.get('confirm_password')
    role = "utilisateur"
    perso = Personne.query.filter_by(mailPers = email).first() #si ça retourne un utilisateur cad que le mail existe déjà 
    if perso :
        flash('Adresse déjà existante !')
        return redirect(url_for("/Connexion"))
    if request.method == 'POST':
        newPers = Personne(mailPers = email, nomPers =nom, prenomPers = prenom,rolePers=role,mdpPers = mdp )
        db.session.add(newPers)
    db.session.commit()

    return render_template(
            "inscription.html",
            title = "S'inscrire")

@app.route("/Abonnement/<int:id_abonnement>", methods=("GET",))
def abonnement(id_abonnement):
    if (id_abonnement == 1):
        return render_template(
            "souscrireAbonnement.html",
            title = "Abonnement",
            typeAbonnement = "Formule gratuite",
            prix = "Gratuite",
            caracteristique = ["Avec pub","5 capteurs disponibles","Acccès au site web"]
    )   
    elif (id_abonnement == 2):
        return render_template(
            "souscrireAbonnement.html",
            title = "Abonnement",
            typeAbonnement = "Formule standard",
            prix = "28.60€/mois",
            caracteristique = ["Sans pub","15 capteurs disponibles","Installation","Maintenance","Localisation disponible"]
    )
    else :
        return render_template(
            "souscrireAbonnement.html",
            title = "Abonnement",
            typeAbonnement = "Formule professionelle",
            prix = "70.90€/mois",
            caracteristique = ["Sans pub","50 capteurs disponibles","Installation","Maintenance","Localisation disponible","+ d'option d'analyse"]
    )   

@app.route("/Contact", methods=("GET",))
def contact_get():
    return render_template(
        "contact.html",
        title = "Contact"
    )


@app.route("/Mesgroupe", methods=("GET",))
def mesgroupes_post():
    return render_template(
        "groupe.html",
        title = "Mes groupes",
        nomGroupe = "Serre A",
        Description = "test",
        Administrateur = "Damien Rian"
    )

@app.route("/Mesgroupe/ajout", methods=("GET",))
def ajouter_groupe_get():
    return render_template(
        "creationGroupe.html",
        title = "Créer votre groupe"
    )

@app.route("/OutilsAnalyse", methods=("GET",))
def outilsAnalyse():
    return render_template(
        "outilAnalyse.html"
    )


@app.route("/detailsCapteur",methods=("GET",) )
def detailsCapteur_parGroupe():
    return render_template(
        "detailsCapteur.html",
        title = "Groupe Serre B",
        title2 = "Capteur présente sur ma carte :",
        capteurs = get_capt_all()
    )

@app.route("/detailsCapteurModifier",methods=("GET",) )
def detailsCapteurModifier_parGroupe():
    return render_template(
        "detailsCapteurModifie.html",
        title = "Groupe Serre B",
        title2 = "Capteur présente sur ma carte :",
        capteurs = get_capt_all()
    )

@app.route("/Motdepasseoublie", methods = ("GET","POST"))
def mdp_get():
    return render_template(
        "resetPassword.html",
        title = "Mot de passe oublié ?"
    )

@app.route("/souscrireAbonnement", methods = ("GET",))
def souscrireAbonnement():
    return render_template(
        "souscrireAbonnement.html",
        title = "Souscrire à un abonnement",
        typeAbonnement = "Formule standard",
        prix = "28.60€/mois",
        caracteristique = ["Sans pub","15 capteurs disponibles","Installation","Maintenance","Localisation disponible"]
    )

@app.route("/Paiement", methods=("GET",))
def paiement_abonnement():
    return render_template(
        "paiement.html",
        title="Souscrire à un abonnement",
        typeAbonnement = "Formule standard"
    )

class CapteurForm(FlaskForm):
    id = StringField("id")
    nom = StringField("nom")
    type = StringField("type")
    car = StringField("car")

@app.route("/AjoutCapteur", methods=("GET","POST",))
def ajoutCapteur():
    c = None
    f = CapteurForm()
    if f.validate_on_submit():
        c = Capteur(
            idCap = f.id.data,
            nomCap = f.nom.data,
            infoCap = f.car.data,
            typeCap = f.type.data,
            statusCap = "En cours"
        )
        db.session.add(c)
        db.session.commit()
        return redirect(url_for("detailsCapteur_parGroupe"))

    return render_template(
        "ajoutCapteur.html",
        title="Ajouter un capteur",
        form = f
    )

@app.route("/ModifierCapteur/<id>", methods=("GET","POST",))
def modifierCapteur(id):
    c = get_capteur_par_id(id)
    f = CapteurForm()
    if f.validate_on_submit():
        if f.id.data != '':
            c.idCap = int(f.id.data)
        if f.nom.data != "":
            c.nomCap = f.nom.data
        if f.car.data != "":
            c.infoCap = f.car.data
        if f.type.data != "":
            c.typeCap = f.type.data
        db.session.commit()
        return redirect(url_for("ficheIdentiteCapteurGroupe", id=c.idCap))

    return render_template(
        "modifierCapteur.html",
        title="Mofier un capteur",
        c = get_capteur_par_id(id),
        form = f
    )

@app.route("/SupprimerCapteur/<id>")
def supprmierCapteur(id):
    c = get_capteur_par_id(id)
    db.session.delete(c)
    db.session.commit()
    return redirect(url_for("detailsCapteur_parGroupe"))


@app.route("/FicheIdentiteCapteur/<id>")
def ficheIdentiteCapteurGroupe(id):
    return render_template(
        "ficheIdentiteCapteur.html",
        title="Fiche identité Capteur",
        capteur = get_capteur_par_id(id)
    )

@app.route("/CharteGraphique", methods=("GET",))
def charteGraphique():
    return render_template(
        "charteGraphique.html",
        title="Charte Graphique"
    )

@app.route("/PresentationGreenSensor", methods=("GET",))
def presentation():
    return render_template(
        "presentation.html",
        title="Présentation de Green Sensor"
    )