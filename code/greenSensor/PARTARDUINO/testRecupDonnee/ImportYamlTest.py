import yaml
import os.path
import serial
import serial.tools.list_ports   # pour la communication avec le port série
from datetime import datetime
import json

def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),p
        )
    )

def recup_port_usb() :
    ports = list(serial.tools.list_ports.comports())
    mData = serial.Serial(ports[1].device,9600)
    for p in ports:
        if 'USB' in p.description :
            mData = serial.Serial(p.device,9600)
    #print(mData)
    return mData

chemin = mkpath("./file.yaml")
chemin2 = mkpath("./file.json")
Data = recup_port_usb()

for k in range(1) :
    line1 = Data.readline() 
    line2 = line1.decode()
    #print("avant", line2)
    line2 = line2[:-2]
    #line2 += "|date=" + str(datetime.now().time())
    #print("après", line2)
    dict_donnee = { b[0] : [b[1]] for b in (x.split("=") for x in line2.split(";")) }
    #print(dict_donnee)
    for i,y in dict_donnee.items():
        #print("ici")
        y.append("date=" + str(datetime.now().time()))
        dict_donnee[i]=y
    dict_file=[]
    dict_file.append(dict_donnee)
    #print("RESULTAT", dict_file) 
    with open(chemin,'r') as yamlfile:
        cur_yaml = yaml.safe_load(yamlfile) # Note the safe_load
        #ETST POUR FAIRE .L TRAVAIL
        #for x in cur_yaml:
        #    print(x)
        #    if x.keys()==dict_file[0].keys():
        #        print("lalal")
                #print(list(dict_file[0].values())[0].append(dict_file.values()))
        #ETST POUR FAIRE L TRAVAIL
        #cur_yaml[0].update(dict_file[0])
        #print(cur_yaml[0])
        #print(dict_file[0])
        for dico in cur_yaml:
            for key in dict_file[0].keys():
                if key in dico.keys():
                    print(dico[key])
                    print(dict_file)
                    dico[key].extend(dict_file[0][key])
                else:
                    cur_yaml[0].update(dict_file[0])


    if cur_yaml:
        with open(chemin,'w') as yamlfile:
            yaml.safe_dump(cur_yaml, yamlfile) # Also note the safe_dump
        with open(chemin2, 'w') as jsonfile:
            json.dump(cur_yaml, jsonfile)
        output = json.dumps(json.load(open('file.json')), indent=2)

#dict_file = [{'sports' : ['soccer', 'football', 'basketball', 'cricket', 'hockey', 'table tennis']},
#{'countries' : ['Pakistan', 'USA', 'India', 'China', 'Germany', 'France', 'Spain']}]


Data.close()