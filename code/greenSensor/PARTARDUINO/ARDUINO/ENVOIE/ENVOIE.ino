#include <DFRobot_sim808.h>
#include <sim808.h>
#include <Arduino.h>
#include <avr/io.h>
//#define SERVER  // prendre soin d'activer  le #define GG_DEBUG dans le .h de  la lib IUTO_SIM808
#include <IUTO_sim808C.h>
#define mySerial Serial1

//########################################
//########################################
#define PHONE_NUMBER "0685703517"  //Put your phone number here!!
//########################################
//########################################

#define Powerkey 12  // PIN used to  wake up shield
#define SIM8008rate 9600 //only 9600 works... Why??
#define STR_LENGTH 200
#define JUMP 3.0
#define MARGINTIME 120

char str[STR_LENGTH];

IUTO_SIM808 sim808(&mySerial);
//******Program states

bool  send_sens = true; // Send SMS with measure // to do
bool  send_gps = false; // Prepare  GPS data into str char*   (to be send by SMS)
bool write_gps = false; // Send to Serial GPS data
bool  Reset = true; // To reset by software Arduino
bool  ResetShield = true; // To reset by software ResetShield

unsigned long starttime;
volatile bool  testSMS=true;
volatile bool testGPS=false;
volatile int  seconds = 0; //make it volatile because it is used inside the interrupt
volatile bool margin=false;
volatile int marginTime=MARGINTIME;
unsigned long lastGPSget = 0;
unsigned char count=1;
float jump=JUMP;

//****** Part Temperature ***********
int sensorPin = 0; 
int valeurPiezo=0 ;
//****** Part Temperature ***********

void setup() {
//******** if arduino is connected to a PC *************
#ifdef SERVER
  Serial.begin(SIM8008rate); //does not need to be same rate than mySerial so may be changed to other values
  while (!Serial);
   GG_DEBUG_PRINTLN("Sim808 est connecte au PC");
#endif
  GG_DEBUG_PRINTLN(Powerkey);
  pinMode(Powerkey, OUTPUT); // needed to wake up shield

  //******** communication to  shield 808 ************* 
  mySerial.begin(SIM8008rate);
  
  //******** Initialize sim808 module *************
  ResetSim808(true); //try to wake up shield  ; true-> a SMS will be send at the end of this process
  sim808_clean_buffer(str,sizeof(str));
  myinterrupts(1);  //1Hz
}

void(* resetFunc) (void) = 0; //declare reset function @ address 0


ISR(TIMER1_COMPA_vect)          // timer compare interrupt service routine
{
  // Do every nbsec second 
  seconds++;
    if (seconds > 4500){
    seconds =0;}
    else{ 
     if(seconds %marginTime ==0){margin=true;}
     if(seconds %20 ==0){testGPS=true;}
      else if(seconds %10 == 0){testSMS=true;}
    }
}

//Main arduino loop
void loop() {
  delay(1);
  //GG_DEBUG_PRINTLN(str); 
  int reading = analogRead(sensorPin);  
  float voltage = reading * 5.0;
  voltage /= 1024.0; 
 
  // now print out the temperature
  float temperatureC = (voltage - 0.5) * 100 ;  //converting from 10 mv per degree wit 500 mV offset
                                               //to degrees ((voltage - 500mV) times 100)
  Serial.print(temperatureC); //Serial.println(" degrees C");

  char buf[50];
  dtostrf(temperatureC, 4, 3, buf);
  sim808.sendSMS(PHONE_NUMBER, buf);
  sim808_clean_buffer(buf,sizeof(buf));
  if (tryReadGPS())
  { 
    sendDataGPS("Last position:");
  }
  else{ snprintf (str, STR_LENGTH,"NoData");
    Serial.print("|");
    Serial.println(str); 
    sim808.sendSMS(PHONE_NUMBER, str);
     sim808_clean_buffer(str,sizeof(str));
     }
  delay(10000);
}

void sendDataGPS(const char * start) {
  ReadDataGPS(start);
  //GG_DEBUG_PRINTLN(str);
  sim808.sendSMS(PHONE_NUMBER, str);
  sim808_clean_buffer(str,sizeof(str));  
}

void ReadDataGPS(const char * start) {
  // compose a string with choosen data
  char str_temp_lat[30];
  dtostrf(sim808.latDec, 9, 6, str_temp_lat);

  char str_temp_lon[30];
  dtostrf(sim808.longDec, 9, 6, str_temp_lon);
  
  snprintf (str, STR_LENGTH,"Lat:%s  Lon:%s", str_temp_lat, str_temp_lon);
  // str is composed , str will be used to be sent or to be displayed
  delay(100);
  Serial.print("|");
  char buf1[20];
  strcat(buf1,str);
  Serial.println(buf1);
  sim808_clean_buffer(buf1,sizeof(buf1));
}
bool tryReadGPS() { //this is called until enough GPS data are readed by arduino to have a geographic  position.
  // Partial information are saved into a global variable so they are not lost from calls to calls to this function.
 if (  sim808.waitGPSready(2)) {
  //the arduino  have a new geographic position!
return true;
  }
return false;
//the arduino may have a new geographic position
}

void myinterrupts(unsigned long frequency){// not less than 0.1 hz
  noInterrupts();// disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;
  //OCR1A = 625;    // 10ms
  //OCR1A =1250000;
  OCR1A = (unsigned long)(62500UL / frequency) ;         // compare match register 16MHz/256/100Hz
        // 16000000/256/100 
          //100Hz= 100/s =toutes les  0,01s = 10ms
        // This value makes ISR execute every 10ms
  TCCR1B |= (1 << WGM12);     // CTC mode
  TCCR1B |= (1 << CS12);      // 256 prescaler 
  TIMSK1 |= (1 << OCIE1A);    // enable timer compare interrupt
  interrupts();  
}

void ResetSim808(bool sendaSMS) {
//  //******** Initialize sim808 data rate but does not work with other values than 9600 :-( *************
 GG_DEBUG_PRINTLN("Initialisation SIM"); 
 sim808.powerReset(Powerkey);  
   
 //GG_DEBUG_PRINTLN("Init rate with 808"); 
while(!sim808.rate(SIM8008rate)) ;
 { GG_DEBUG_PRINTLN("Turn Rate");
//
   mySerial.flush();
   mySerial.begin(SIM8008rate);
    while( mySerial.available())  {GG_DEBUG_PRINTLN("Purge");mySerial.read();}
    //delay(1000);
  }

//******** Wait a connection to the shield establised *************  
while(!sim808.testSIM808(1000,false)) {delay(1000);}
  
sim808.deleteAllSMS(10000); // Delete message to free memory, may be important not to saturate memory
  
 GG_DEBUG_PRINTLN("Allumage du module GPS");
//
//  //************* Turn on the GPS power************
 
 while(!sim808.poweronGPS() ) {delay(1000);}
   
   ; // wake up GPS GNSS module module
  
  if (sendaSMS){
    snprintf (str,STR_LENGTH, "Allumage de l'Arduino, préparation à l'envois de donnees");
    delay(10000);
    sim808.sendSMS(PHONE_NUMBER, str);
    sim808_clean_buffer(str,sizeof(str));
  }
}
