#include <DFRobot_sim808.h>
#include <sim808.h>
#include <Arduino.h>
#include <avr/io.h>
#include <IUTO_sim808C.h>
#define mySerial Serial1

#define SIM8008rate 9600 
#define STR_LENGTH 200

char str[STR_LENGTH];

IUTO_SIM808 sim808(&mySerial);
//******Program states*

void setup() {
 
  //******** communication to  shield 808 ************* 
  mySerial.begin(SIM8008rate);
  while (!mySerial);
  
}

void loop() {
  delay(1);  
  if(sim808.readSMS(1, str, STR_LENGTH,10000)){
    sim808.deleteAllSMS(7000); // Delete message to free memory, may be important not to saturate memory
    // depends of value into the SMS we choose a suitable state for the program
    Serial.println(str);
    sim808_clean_buffer(str,sizeof(str));
  }
}
