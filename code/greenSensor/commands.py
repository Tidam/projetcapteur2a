import click
from .app import app , db
from .models import Capteur, Personne, Abonnement

@app.cli.command()
def syncdb():
    '''creer table manquante'''
    db.create_all()


@app.cli.command()
def resetdb():
    '''vide la base de donnée et synchronise'''
    db.drop_all()
    db.create_all()

@app.cli.command()
#@click.argument('filename')
def loaddb():
    db.create_all()

    import yaml
    personneFile = yaml.safe_load(open("greenSensor/donneesYml/personne.yml"))
    abonnementFile = yaml.safe_load(open("greenSensor/donneesYml/abonnement.yml"))
    capteurFile = yaml.safe_load(open("greenSensor/donneesYml/capteur.yml"))
    
    for a in abonnementFile: #abonnement 
        abo = Abonnement(nomAbo = a["nomAbo"],
                         prixAbo  = a["prixAbo"],
                         infoAbo = a["infoAbo"],
                         idAbo = a["numAbo"])
        db.session.add(abo)
    db.session.commit()
    
    for p in personneFile:
        perso = Personne(nomPers = p["nomPers"],
                            prenomPers = p["prenomPers"],
                            mailPers = p["mailPers"],
                            photoProfilPers = "coucou",
                            mdpPers = p["mdpPers"],
                            rolePers = p["rolePers"],
                            aboId = p["aboId"]
                            )
        db.session.add(perso)
    db.session.commit()

    for c in capteurFile:
        capt = Capteur(idCap = c["id"],
                            nomCap = c["nom"],
                            infoCap = c["info"],
                            typeCap= c["type"],
                            geolocalCapX = c["x"],
                            geolocalCapY = c["y"],
                            statusCap = c["status"]
                            )
        db.session.add(capt)
    db.session.commit()

    
@app.cli.command()
@click.argument('mailpers')
@click.argument('telpers')
@click.argument('nompers')
@click.argument('prenompers')
@click.argument('photoprofilpers')
@click.argument('mdppers')
@click.argument('roleperso')
@click.argument('aboid')
def newuser(mailpers, telpers,nompers,prenompers,photoprofilpers,mdppers,roleperso,aboid):
    '''Adds a new user.'''
    from hashlib import sha256
    m = sha256()
    m.update(mdppers.encode())
    u = Personne(mailPers= mailpers, telPers=int(telpers), nomPers= nompers, prenomPers=prenompers, photoProfilPers=photoprofilpers, mdpPers=m.hexdigest(),rolePers= roleperso,aboId=int(aboid))
    db.session.add(u)
    db.session.commit()